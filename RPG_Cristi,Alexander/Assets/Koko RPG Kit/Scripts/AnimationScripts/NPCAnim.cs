﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnim : MonoBehaviour
{
    Animator anim;

    public GameObject Idle, Talk;
    // Use this for initialization
    void Start()
    {
        anim = this.GetComponent<Animator>();

        anim.SetInteger("AIndex", 0);
    }

    // Update is called once per frame
    void Update()
    {
        CheckButtonPressed();
    }

    void CheckButtonPressed()
    {
        if (Idle.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 1);
        }

        if (Talk.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 2);
        }
    }
}