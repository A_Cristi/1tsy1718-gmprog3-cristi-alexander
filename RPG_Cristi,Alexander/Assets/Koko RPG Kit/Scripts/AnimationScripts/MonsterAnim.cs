﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnim : MonoBehaviour {
    Animator anim;

    public GameObject Idle, Walk, ATKRun, Dam, ATK, Skill, Dead;
    // Use this for initialization
    void Start () {

        anim = this.GetComponent<Animator>();

        anim.SetInteger("AIndex", 0);
    }
	
	// Update is called once per frame
	void Update () {
        CheckButtonPressed();
    }

    void CheckButtonPressed()
    {
        if (Idle.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 1);
        }

        if (Walk.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 2);
        }

        if (ATKRun.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 3);
        }

        if (Dam.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 4);
        }

        if (ATK.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 5);
        }

        if (Skill.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 6);
        }

        if (Dead.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 7);
        }
    }
}
