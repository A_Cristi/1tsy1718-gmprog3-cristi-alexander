﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAnim : MonoBehaviour {
    
    Animator anim;

    public GameObject Idle, DrawBlade, Run, PutBlade, ATKRun, Dam, ATK, Skill, ATKStand, Dead, Combo;

	// Use this for initialization
	void Start () {
        anim = this.GetComponent<Animator>();

        anim.SetInteger("AIndex", 0);
	}
	
	// Update is called once per frame
	void Update () {
        CheckButtonPressed();

    }

    void CheckButtonPressed()
    {
       if (Idle.GetComponent<ButtonClick>().isClicked == true)
       {
            anim.SetInteger("AIndex", 0);
       }

       if (DrawBlade.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 2);
        }

        if (Run.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 1);
        }

        if (PutBlade.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 3);
        }

        if (ATKRun.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 4);
        }

        if (Dam.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 5);
        }

        if (ATK.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 4);
        }

        if (Skill.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 6);
        }

        if (ATKStand.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 7);
        }

        if (Dead.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 8);
        }

        if (Combo.GetComponent<ButtonClick>().isClicked == true)
        {
            anim.SetInteger("AIndex", 9);
        }
    }

}
