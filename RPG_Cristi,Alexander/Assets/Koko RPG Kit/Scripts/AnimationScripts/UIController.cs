﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

	public ButtonClick PlayerButton, MonButton, NPCButton;
    int Checker;

    public GameObject PlayerHolder, MonHolder, NPCHolder;

    GameObject MainCamera;

    public GameObject PlayerCamPos, MonsterCamPos, NPCCamPos;

    // Use this for initialization
    void Start()
    {
      

        MainCamera = GameObject.Find("Main Camera");
        Checker = 0;
    }

    // Update is called once per frame
    void Update()
    {
        CheckButton();
    }

    void CheckButton()
    {
       
        if (MonButton.GetComponent<ButtonClick>().isClicked == true)
        {
            PlayerHolder.SetActive(false);
            MonHolder.SetActive(true);
            NPCHolder.SetActive(false);
            Debug.Log("Monster");
            MoveCamera(2);
        }

        if (NPCButton.GetComponent<ButtonClick>().isClicked == true)
        {
            PlayerHolder.SetActive(false);
            MonHolder.SetActive(false);
            NPCHolder.SetActive(true);
            Debug.Log("NPC");
            MoveCamera(3);
        }

		if (PlayerButton.isClicked)
		{
			PlayerHolder.SetActive(true);
			MonHolder.SetActive(false);
			NPCHolder.SetActive(false);
			Debug.Log("Player");
			MoveCamera(1);
		}


    }

    void MoveCamera(int c)
    {
        switch (c)
        {
            case 1:
                MainCamera.transform.position = PlayerCamPos.transform.position;
                break;

            case 2:
                MainCamera.transform.position = MonsterCamPos.transform.position;
                break;

            case 3:
                MainCamera.transform.position = NPCCamPos.transform.position;
                break;
        }

    }

}
