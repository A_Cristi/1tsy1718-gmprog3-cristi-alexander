﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterSpawner : MonoBehaviour {

    public GameObject GoblinPrefab, SlimePrefab, WildeSauPrefab;

    public float RadiusAroundPlayer;
    public float IntervalSeconds;
    public int MaxNumOfMonsters;

    public int monsCount;

    Vector3 pos;
    GameObject Player;   
   

    float timer;

    // Use this for initialization
    void Start () {
        Player = GameObject.Find("Koko");
        timer = IntervalSeconds;
        monsCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
        //MonstersInScene = GameObject.FindGameObjectsWithTag("Enemy");

        timer += Time.deltaTime;

        if (timer >= IntervalSeconds)
        {
            Spawn();           
            timer = 0;
        }

    }

    Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
        //https://forum.unity.com/threads/solved-random-wander-ai-using-navmesh.327950/
    }

    void Spawn()
    {
        int i = Random.Range(1, 100);

        if (Player == null)
            Player = GameObject.FindGameObjectWithTag("Player");

        Vector3 newPos = RandomNavSphere(Player.transform.position, RadiusAroundPlayer, -1);
        //Debug.Log(newPos);        
        
        if (monsCount <= MaxNumOfMonsters )
        {            
            if (i < 33)
            {
                Instantiate(GoblinPrefab, newPos, transform.rotation);
            }

            else if (i < 66)
            {
                Instantiate(SlimePrefab, newPos, transform.rotation);
            }

            else
            {
                Instantiate(WildeSauPrefab, newPos, transform.rotation);
            }
            monsCount++;
        }
    }

}
    