﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterStats : MonoBehaviour {

    public float BaseVitality, BaseStrength;

    public float EXP;

    public Image HPBar;
    float maxHP;   

    float vitality, str;
    public float HP, AP;

    public MonsterAI AI;
    MonsterAnimState animState;
    

    // Use this for initialization
    void Start () {
        //HPBar = GameObject.Find("Canvas/HP Bar").GetComponent<Image>();
        HPBar.fillAmount = 1.0f;        
        animState = GetComponent<MonsterAnimState>();
        
        vitality = BaseStrength;
        str = BaseStrength;
        ComputeStats();
        maxHP = HP;
    }
	
	// Update is called once per frame
	void Update () {   

        if (HP <= 0.0f)
        {           
            AI.player.GetComponent<PlayerStats>().KilledEnemy(EXP, gameObject);
            GetComponent<MonsterDrops>().DropItem();
            Destroy(gameObject);
        }
        else
        {
            UpdateHP();
        }  
	}

    void ComputeStats()
    {
        HP = vitality * 10;
        AP = str * 10;        
    }

    public void TakeDMG(float dmg)
    {        
        HP -= dmg;
        animState.CurState = MonsterAnimState.MonsAnimState.TakeDam;       
    }

    public void DamageTo()
    {
        AI.player.GetComponent<PlayerStats>().TakeDmg(AP);        
    }

    void UpdateHP()
    {
        if(gameObject != null)
            HPBar.fillAmount = HP / maxHP;
        //Debug.Log(HPBar.fillAmount);
    }

}
