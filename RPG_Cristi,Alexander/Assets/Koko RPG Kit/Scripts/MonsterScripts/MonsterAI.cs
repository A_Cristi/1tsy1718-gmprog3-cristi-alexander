﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
    
public class MonsterAI : MonoBehaviour {
    public enum MonType
    {
        Slime,
        Goblin,
        WildeSau
    }

    enum AIState
    {
        Idle,
        Patrol,
        Chase,
        Attack
    }
   
    public MonType MonsterType;

    MonsterAnimState monState;

    AIState curState;
    float distanceToPlayer;
    public float AggroRange;

    bool withinRange;
    bool hasArrived;

    Vector3 des;

    NavMeshAgent agent;    

    public GameObject player;
    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        withinRange = false;

        monState = GetComponent<MonsterAnimState>();

        player = GameObject.FindGameObjectWithTag("Player");

        monState.CurState = MonsterAnimState.MonsAnimState.Idle;

        hasArrived = false;
        curState = AIState.Patrol;

        GetNewRandomPos();
    }
	
	// Update is called once per frame
	void Update () {
        CheckPlayerDeath();
        CheckIfPlayerInside();
        CheckState();
    }     
    
    void CheckPlayerDeath()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
    } 

    void CheckIfPlayerInside()
    {
        Collider[] inside = Physics.OverlapSphere(transform.position, AggroRange);

        if (player == null)
        {
            curState = AIState.Patrol;
            return;          
        }

        for (int i = 0; i < inside.Length; i++)
        {
            if (inside[i].tag == "Player")
            {
                //Debug.Log("Player Found!");
                distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
                //Debug.Log(distanceToPlayer);
                

                if (distanceToPlayer < 1.0f)
                {
                    curState = AIState.Attack;
                }

                else
                {
                    curState = AIState.Chase;
                }
            } 
        }

        

        
    }

    void CheckState()
    {
        switch(curState)
        {
            case AIState.Chase:
                Chase();
                break;

            case AIState.Patrol:
                Patrol();
                break;

            case AIState.Attack:
                Attack();
                break;
        }
    }

    void Patrol()
    {
        Debug.Log("Patrolling");
                
        if (hasArrived == false)
        {            
            if (Vector3.Distance(transform.position, des) > 1.5f)
            {
                agent.destination = des;
            }
            else
            {
                hasArrived = true;
            }
        }

        if(hasArrived == true)
        {
            GetNewRandomPos();
            hasArrived = false;
        }
        monState.CurState = MonsterAnimState.MonsAnimState.Moving;
    }

    void GetNewRandomPos()
    {
        des = Random.insideUnitSphere * 4 + transform.position;
        des.y = Mathf.Clamp(des.y, 0, 1);
        //Debug.Log(des);
    }

    void Chase()
    {
        Debug.Log("Chasing");
        //animator.SetInteger("AIndex", 2);
        monState.CurState = MonsterAnimState.MonsAnimState.Moving;
        agent.destination = player.transform.position;  
    }   

    void Attack()
    {
        Debug.Log("Attack");
        //animator.SetInteger("AIndex", 3);
        monState.CurState = MonsterAnimState.MonsAnimState.Attacking;

        //if(player == null)
        //{
        //    curState = AIState.Patrol;
        //}

        if(player.GetComponent<PlayerStats>().GetHP() <= 0.0f || player == null)
        {
            curState = AIState.Patrol;
        }
    }       

    private void OnDrawGizmosSelected()
    {
     Gizmos.color = Color.red;     
     Gizmos.DrawWireSphere(transform.position, AggroRange);
    }
}
