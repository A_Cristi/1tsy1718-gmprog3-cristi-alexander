﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDrops : MonoBehaviour {

    public List<GameObject> DropList = new List<GameObject>();

    GameObject itemToDrop = null;

    // Use this for initialization
    void Start () {
        SelectDroppable();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SelectDroppable()
    {
        int f = Random.Range(0, DropList.Count);

        itemToDrop = DropList[f];
    }

    public void DropItem()
    {
        Instantiate(itemToDrop, transform.position, Quaternion.identity);
    }
}
