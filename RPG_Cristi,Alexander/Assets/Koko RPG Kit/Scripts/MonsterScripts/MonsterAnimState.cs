﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimState : MonoBehaviour {
    Animator anim;
    public enum MonsAnimState
    {
        Moving,
        Attacking,
        Idle,
        TakeDam
    }
    public MonsAnimState CurState;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        CheckCurState();
    }

    void CheckCurState()
    {
        switch (CurState)
        {
            case MonsAnimState.Idle:
                Idle();
                break;

            case MonsAnimState.Moving:
                Moving();
                break;

            case MonsAnimState.Attacking:
                Attack();
                break;

            case MonsAnimState.TakeDam:
                TakeDam();
                break;
        }

    }

    void Idle()
    {
        anim.SetInteger("AIndex", 1);
    }

    void Moving()
    {
        anim.SetInteger("AIndex", 2);
    }

    void Attack()
    {
        anim.SetInteger("AIndex", 3);
    }

    void TakeDam()
    {
        anim.SetInteger("AIndex", 4);
    }
}
