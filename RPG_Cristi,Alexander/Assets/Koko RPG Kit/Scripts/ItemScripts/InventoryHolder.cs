﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryHolder : MonoBehaviour {

    public List<GameObject> PInvent = new List<GameObject>();
    public InventoryUI iUI;

    GameObject Player;
    // Use this for initialization
    void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");

	}
	
	// Update is called once per frame
	void Update () {
		if (Player == null)
        {
            FindPlayer();
        }

    }

    void FindPlayer()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    public void ActivateHP()
    {
        for (int i = 0; i < PInvent.Count;i++)
        {
            if(PInvent[i].GetComponent<Item>().myType == Item.ItemType.HP)
            {
                PInvent[i].GetComponent<HPPotion>().Effect();
                PInvent.RemoveAt(i);
                return;
            }
        }
    }

    public void ActivateMP()
    {
        for (int i = 0; i < PInvent.Count; i++)
        {
            if (PInvent[i].GetComponent<Item>().myType == Item.ItemType.MP)
            {
                PInvent[i].GetComponent<MPPotion>().Effect();
                PInvent.RemoveAt(i);
                return;
            }
        }
    }

    public void ActivateVITB()
    {
        for (int i = 0; i < PInvent.Count; i++)
        {
            if (PInvent[i].GetComponent<Item>().myType == Item.ItemType.VITB)
            {
                PInvent[i].GetComponent<BuffPotion>().Effect();
                PInvent.RemoveAt(i);
                return;
            }
        }
    }

    public void ActivateSTRB()
    {
        for (int i = 0; i < PInvent.Count; i++)
        {
            if (PInvent[i].GetComponent<Item>().myType == Item.ItemType.STRB)
            {
                PInvent[i].GetComponent<BuffPotion>().Effect();
                PInvent.RemoveAt(i);
                return;
            }
        }
    }

    public void ActivateINTB()
    {
        for (int i = 0; i < PInvent.Count; i++)
        {
            if (PInvent[i].GetComponent<Item>().myType == Item.ItemType.INTB)
            {
                PInvent[i].GetComponent<BuffPotion>().Effect();
                PInvent.RemoveAt(i);
                return;
            }
        }
    }

    public void ActivateSPRB()
    {
        for (int i = 0; i < PInvent.Count; i++)
        {
            if (PInvent[i].GetComponent<Item>().myType == Item.ItemType.SPRB)
            {
                PInvent[i].GetComponent<BuffPotion>().Effect();
                PInvent.RemoveAt(i);
                return;
            }
        }
    }

    public void AddItem(GameObject toAdd)
    {
        PInvent.Add(toAdd);
    }

    public void AddHPP(GameObject item)
    {
        iUI.AddHPpotion();
        AddItem(item);
    }

    public void AddMPP(GameObject item)
    {
        iUI.AddMPpotion();
        AddItem(item);
    }

    public void AddVITB(GameObject item)
    {
        iUI.AddVitalityBuff();
        AddItem(item);
    }

    public void AddSTRB(GameObject item)
    {
        iUI.AddStrengthBuff();
        AddItem(item);
    }

    public void AddSPRB(GameObject item)
    {
        iUI.AddSpiritBuff();
        AddItem(item);
    }

    public void AddINTB(GameObject item)
    {
        iUI.AddIntBuff();
        AddItem(item);
    }
}
