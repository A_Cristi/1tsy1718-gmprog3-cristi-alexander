﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvenActivate : MonoBehaviour {

    bool IsHidden;

    RectTransform thisRect;
    Vector3 move;

    // Use this for initialization
    void Start () {
        IsHidden = false;
        thisRect = gameObject.GetComponent<RectTransform>();
        move = new Vector3(-450, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.I))
        {
            if (IsHidden == false)
            {
                thisRect.localPosition += move;
                IsHidden = true;
            }
            else
            {
                thisRect.localPosition -= move;
                IsHidden = false;
            }
        }

    }
}
