﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {
    public string itemName;

    public enum ItemType {
        HP,
        MP,
        Weapon,
        VITB,
        STRB,
        INTB,
        SPRB
    }
    public PlayerStats pStats;
    public ItemType myType;

    // Use this for initialization
    void Start () {
        pStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void Effect()
    {
        
    }
}
