﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPurse : MonoBehaviour {

    PlayerStatsHolder PSH;
    public int Amt;
    private void Start()
    {
        PSH = GameObject.Find("StatsHolder").GetComponent<PlayerStatsHolder>();
    }

    public void AddCash()
    {
        PSH.gold += Amt;
        Destroy(gameObject);
    }
}
