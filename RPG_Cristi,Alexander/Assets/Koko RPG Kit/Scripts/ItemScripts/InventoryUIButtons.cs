﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUIButtons : MonoBehaviour {
    InventoryUI iUI;
    InventoryHolder iH;

    PlayerStatsHolder PSH;

    Store store;

    public enum UIType
    {
        HP,
        MP,
        VITB,
        STRB,
        INTB,
        SPRB
    }

    public UIType type;
    private void Start()
    {
        store = GameObject.FindGameObjectWithTag("STORE").GetComponent<Store>();
        PSH = GameObject.Find("StatsHolder").GetComponent<PlayerStatsHolder>();
        iH = GameObject.Find("InventoryHolder").GetComponent<InventoryHolder>();
        iUI = GameObject.Find("Inventory UI").GetComponent<InventoryUI>();
    }

    public void Health()
    {        
        iH.ActivateHP();
        Destroy(gameObject);
    }

    public void Mana()
    {
        iH.ActivateMP();
        Destroy(gameObject);
    }

    public void VITB()
    {
        iH.ActivateVITB();
        Destroy(gameObject);    
    }

    public void STRB()
    {
        iH.ActivateSTRB();
        Destroy(gameObject);
    }

    public void INTB()
    {
        iH.ActivateINTB();
        Destroy(gameObject);
    }

    public void SPRB()
    {
        iH.ActivateSPRB();
        Destroy(gameObject);
    }

    public void Sold()
    {        
        if (store.CanOpen)
        {
            PSH.gold += 100;
            Destroy(gameObject);
        }
        else
        {
            Debug.Log("NO SELL");
        }
    }
}
