﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffPotion : Item {

    public enum StatBuff
    {
        Vitality,
        Strength,
        Spirit,
        Intelligence
    }

    public StatBuff BuffStat;
    public float Duration;
    public float AmountBuff;

    PlayerStatsHolder PSH;

	// Use this for initialization
	void Start () {
        PSH = GameObject.Find("StatsHolder").GetComponent<PlayerStatsHolder>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Effect()
    {
        base.Effect();
        switch (BuffStat)
        {
            case StatBuff.Vitality:
                PSH.VITBuffActive = true;               
                break;

            case StatBuff.Strength:
                PSH.STRBuffActive = true;               
                break;

            case StatBuff.Spirit:
                PSH.SPRBuffActive = true;
                break;

            case StatBuff.Intelligence:
                PSH.INTBuffActive = true;
                break;
        }


    }
}
