﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPPotion : Item
{
    public float HealAmt;
    // Use this for initialization
    void Start () {
        pStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Effect()
    {
        base.Effect();
        pStats.AddHP(HealAmt);
        Debug.Log("HP");

    }
}
