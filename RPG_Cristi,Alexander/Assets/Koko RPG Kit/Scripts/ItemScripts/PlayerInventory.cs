﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {

    InventoryHolder IH;

	// Use this for initialization
	void Start () {
        IH = GameObject.Find("InventoryHolder").GetComponent<InventoryHolder>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Entered");
        switch(other.tag)
        {
            case "HP": 
                IH.AddHPP(other.gameObject);
                //Destroy(other.gameObject);
                other.gameObject.SetActive(false);
                break;

            case "MP":
                IH.AddMPP(other.gameObject);
                //Destroy(other.gameObject);
                other.gameObject.SetActive(false);
                break;

            case "VITB":
                IH.AddVITB(other.gameObject);
                //Destroy(other.gameObject);
                other.gameObject.SetActive(false);
                break;

            case "STRB":
                IH.AddSTRB(other.gameObject);
                //Destroy(other.gameObject);
                other.gameObject.SetActive(false);
                break;

            case "INTB":
                IH.AddINTB(other.gameObject);
                //Destroy(other.gameObject);
                other.gameObject.SetActive(false);
                break;

            case "SPRB":
                IH.AddSPRB(other.gameObject);
                //Destroy(other.gameObject);
                other.gameObject.SetActive(false);
                break;

            case "Coins":
                other.GetComponent<CoinPurse>().AddCash();
                break;

        }
    }
}
