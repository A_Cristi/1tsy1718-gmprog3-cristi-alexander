﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour {

    public GameObject HPSprite, MPSprite;
    public GameObject VITB, STRB, SPRB, INTB;


    //public List<GameObject> InvenUI = new List<GameObject>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //public void ActivateHPP()
    //{
    //    Debug.Log("found");
    //    for (int i = 0; i < InvenUI.Count; i++)
    //    {
    //        if (InvenUI[i] == null)
    //        {
    //            InvenUI.RemoveAt(i);
    //            break;
    //        }

    //        if(InvenUI[i].GetComponent<InventoryUIButtons>().type == InventoryUIButtons.UIType.HP)
    //        {
    //            InvenUI.RemoveAt(i);
    //            return;
    //        }
    //    }
    //}

    public void AddHPpotion()
    {
        GameObject newObj;
        newObj = (GameObject)Instantiate(HPSprite, transform);
        //InvenUI.Add(newObj);
        Debug.Log("Added Item");
    }

    public void AddMPpotion()
    {
        GameObject newObj;
        newObj = (GameObject)Instantiate(MPSprite, transform);
        //InvenUI.Add(newObj);
        Debug.Log("Added Item");
    }

    public void AddVitalityBuff()
    {
        GameObject newObj;
        newObj = (GameObject)Instantiate(VITB, transform);
        //InvenUI.Add(newObj);
        Debug.Log("Added Item");
    }

    public void AddStrengthBuff()
    {
        GameObject newObj;
        newObj = (GameObject)Instantiate(STRB, transform);
        //InvenUI.Add(newObj);
        Debug.Log("Added Item");
    }

    public void AddIntBuff()
    {
        GameObject newObj;
        newObj = (GameObject)Instantiate(INTB, transform);
        //InvenUI.Add(newObj);
        Debug.Log("Added Item");
    }

    public void AddSpiritBuff()
    {
        GameObject newObj;
        newObj = (GameObject)Instantiate(SPRB, transform);
        //InvenUI.Add(newObj);
        Debug.Log("Added Item");
    }
}
