﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : Skill {
    public int HealAmt;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Effect()
    {
        base.Effect();
        PStats.AddHP(HealAmt);
    }

    protected override void CD()
    {
        base.CD();
    }
}
