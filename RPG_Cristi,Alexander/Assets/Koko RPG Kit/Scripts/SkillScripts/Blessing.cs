﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blessing : Skill {
    
    protected override void Start()
    {
        base.Start();
        PSH = GameObject.Find("StatsHolder").GetComponent<PlayerStatsHolder>();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Effect()
    {
        base.Effect();
        PSH.Blessing();
    }

    protected override void CD()
    {
        base.CD();
    }
}
