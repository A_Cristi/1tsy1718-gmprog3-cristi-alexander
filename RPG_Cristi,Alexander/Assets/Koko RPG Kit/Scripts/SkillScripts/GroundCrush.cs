﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCrush : Skill
{
    public float AOERad;

    PlayerAttack PAtk;
    protected override void Start()
    {
        PAtk = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAttack>();
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Effect()
    {
        base.Effect();       
        PAtk.AreaAttack(AOERad);
    }

    protected override void CD()
    {
        base.CD();
    }    
}
