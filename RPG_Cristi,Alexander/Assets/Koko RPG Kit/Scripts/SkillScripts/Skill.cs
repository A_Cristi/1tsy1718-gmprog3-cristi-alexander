﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skill : MonoBehaviour
{
    public int MPCost;
    public Text MPCostText;

    public float Cooldown;
    protected float CDText;


    public KeyCode AssignedKey;
    public GameObject CDPanel;
    public Text CDCounter;

    protected bool IsCD = false;

    protected PlayerStats PStats;
    protected PlayerStatsHolder PSH;
    // Use this for initialization
    protected virtual void Start()
    {
        CDText = Cooldown;
        CDPanel.SetActive(false);

    }

    // Update is called once per frame
    protected virtual void Update()
    {
        MPCostText.text = MPCost.ToString();

        if (IsCD == false)
        {
            if (Input.GetKeyDown(AssignedKey))
            {
                Effect();
            }
        }

        else
        {
            CDText -= Time.deltaTime;
            if (CDText <= 0)
            {
                CDText = Cooldown;
                IsCD = false;
                CDPanel.SetActive(false);
            }
        }

        CDCounter.text = CDText.ToString();

    }

    public virtual void Effect()
    {
        PStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        PStats.UseSkill(MPCost);
        Debug.Log("skill");
        CD();
    }

    protected virtual void CD()
    {
        CDPanel.SetActive(true);
        IsCD = true;


    }
}
