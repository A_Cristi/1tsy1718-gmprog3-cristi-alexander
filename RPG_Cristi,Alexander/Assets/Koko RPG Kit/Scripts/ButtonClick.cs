﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClick : MonoBehaviour {
    Button ThisButton;

    public bool isClicked = false;
    // Use this for initialization
    void Start () {
        Button ThisButton = gameObject.GetComponent<Button>();
        ThisButton.onClick.AddListener(TaskOnClick);
    }
	
	// Update is called once per frame
	float timer = 0;
	void Update () 
	{
        Timer();
	}

    void Timer()
    {
        timer += Time.deltaTime;
        if (timer >= .1f)
        {
            isClicked = false;
            timer = 0;
        }
    }

    void TaskOnClick()
    {
        isClicked = true;
        if (isClicked == true)
        {
			Debug.Log("You have clicked the button!" + this.gameObject.name);
        }
    }
}
