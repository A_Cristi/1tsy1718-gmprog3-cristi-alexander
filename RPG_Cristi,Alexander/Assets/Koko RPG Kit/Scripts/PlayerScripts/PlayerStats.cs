﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

    PlayerState pstate;
    PlayerStatsHolder stats;
    PlayerQuestTracker PQT;

    float maxHP;
    float maxMP, MP, Matk, MPregen;

    float vitality, str;
    float HP, AP;

    float timer = 1.0f;   

    // Use this for initialization
    void Start () {        
        pstate = GetComponent<PlayerState>();
        stats = GameObject.Find("StatsHolder").GetComponent<PlayerStatsHolder>();
        PQT = GetComponent<PlayerQuestTracker>();

        //HP = stats.BaseVitality;
        //AP = stats.BaseStrength;
        
        

        vitality = stats.BaseStrength;
        str = stats.BaseStrength;
        ComputeStats();
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        vitality = stats.BaseVitality;
        str = stats.BaseStrength;        
        stats.curHP = HP;
        stats.maxHP = maxHP;
        stats.atkdmg = AP;

        stats.CurMP = MP;
        stats.MaxMP = maxMP;
        stats.MPRegen = MPregen;
        stats.Matk = Matk;

        RegenMana();
        stats.UpdateMPBar(maxMP, MP);
        stats.UpdateHPBar(maxHP, HP);

        if (MP <= 0)
        {
            MP = 0.0f;
        }

        if (HP >= maxHP)
        {
            HP = maxHP;
        }
    }

    void ComputeStats()
    {
        HP = stats.BaseVitality * 10;
        AP = stats.BaseStrength * 10;
        maxHP = HP;

        MPregen = (stats.BaseSpirit / 10);
        MP = stats.BaseSpirit * 100;
        maxMP = MP;

        Matk = stats.BaseInt * 3;
    }

    public void CheckBuff()
    {
        maxHP = stats.BaseVitality * 10;
        AP = stats.BaseStrength * 10;

        maxMP = stats.BaseSpirit * 100;
        MPregen = (stats.BaseSpirit / 10);
        Matk = stats.BaseInt * 3;
        Debug.Log("Updated");
    }

    public void UseSkill(int MPCost)
    {
        MP -= MPCost;
    }

    public void lvlup()
    {
        Debug.Log("lvl");
        ComputeStats();
    }    

    public float GetAP()
    {
        return AP;
    }

    public void TakeDmg(float dmg)
    {
        HP -= dmg;
        //Debug.Log(HP);
        stats.UpdateHPBar(maxHP, HP);
        pstate.CurState = PlayerState.PState.TakeDam;
    }

    public void KilledEnemy(float exp, GameObject mons)
    {
        stats.curExp += exp;
        PQT.KillTrack(mons);
    }

    public void RegenMana()
    {
        if (MP < maxMP)
        {
            timer -= Time.deltaTime;
            if (timer < 0.0f)
            {
                MP += MPregen;
                timer = 1.0f;

            }
        }
    }   

    public void AddHP(float HAmt)
    {

        if (HP >= maxHP)
        {
            HP = maxHP;
        }
        else
        {
            HP += HAmt;
            if (HP >= maxHP)
            {
                HP = maxHP;
            }
        }

        stats.UpdateHPBar(maxHP, HP);
    }

    public void AddMP(float HAmt)
    {

        if (MP >= maxMP)
        {
            MP = maxMP;
        }
        else
        {
            MP += HAmt;
            if (MP >= maxMP)
            {
                MP = maxMP;
            }
        }
    }

    public void Buffed()
    {
        AP = stats.BaseStrength * 10;
        Matk = stats.BaseInt * 3;
    }

    public float GetHP()
    { return HP;}
}
