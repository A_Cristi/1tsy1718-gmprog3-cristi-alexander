﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

    PlayerStats pstats;
    PlayerState pstate;
    PlayerMovement pmove;

    float atkpwr;


    GameObject target;
    // Use this for initialization
    void Start () {
        target = null;
        pstats = GetComponent<PlayerStats>();
        pstate = GetComponent<PlayerState>();
        pmove = GetComponent<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        CheckClick();
        atkpwr = pstats.GetAP();
        CheckTarget();
    }

    void CheckClick()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Enemy")
                {
                    target = hit.transform.gameObject;
                    pmove.MoveToTarget(target);
                }
            }
        }

        if (target != null)
        {
            float Dis = Vector3.Distance(transform.position, target.transform.position);
            //Debug.Log(Dis);
            if (Dis < 3 && target != null)
            {
                pstate.CurState = PlayerState.PState.Attacking;
            }
        }  
       
        if (Input.GetMouseButton(1))
        {
            target = null;
            pstate.CurState = PlayerState.PState.Moving;
        }
    }

    void CheckTarget()
    {
        if (target == null && pstate.CurState == PlayerState.PState.Attacking)
        {
            pstate.CurState = PlayerState.PState.Idle;
        }
    }

    public void DamageTo()
    {
        if(target != null)
        {
            target.GetComponent<MonsterStats>().TakeDMG(atkpwr);
        }
       
    }

    public void AreaAttack(float rad)
    {
        Collider[] s = Physics.OverlapSphere(transform.position, rad);
        pstate.CurState = PlayerState.PState.Skill;

        foreach (Collider col in s)
        {
            if (col.tag == "Enemy")
            {
                col.GetComponent<MonsterStats>().TakeDMG(atkpwr);
                Debug.Log("AOE");
            }

        }
    }

    public void StartEffect() { }
    public void StopEffect() { }
}
