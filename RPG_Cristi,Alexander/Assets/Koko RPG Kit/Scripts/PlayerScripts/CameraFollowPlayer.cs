﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour {

    public GameObject target;
    public float speed;
        
    private Vector3 targetOffset;
    // Use this for initialization 
    void Start () {
        targetOffset = transform.position - target.transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        if (target == null)
        {
            FindPlayer();
        }

        Vector3 PlayerLock = transform.position;
        PlayerLock.y = Mathf.Clamp(transform.position.y, 13.0f, 13.1f); 
        transform.position = PlayerLock;
        
        Vector3 OffsetPos = target.transform.position + targetOffset;

        //transform.position = Vector3.MoveTowards(transform.position, OffsetPos, speed * Time.deltaTime);
        transform.position = Vector3.Lerp(transform.position, OffsetPos, speed * Time.deltaTime);
          
    }

    void FindPlayer()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

}
