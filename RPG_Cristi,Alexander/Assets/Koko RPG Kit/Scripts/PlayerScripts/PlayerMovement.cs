﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class PlayerMovement : MonoBehaviour {
    public GameObject Marker;
    NavMeshAgent agent;

    Animator anim;
    GameObject ActiveMarker;

    PlayerState playerState;

    float dist;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        ActiveMarker = null;

        playerState = GetComponent<PlayerState>();  

        anim = this.GetComponent<Animator>();

        //anim.SetInteger("AIndex", 0);
        playerState.CurState = PlayerState.PState.Idle;
    }
	
	// Update is called once per frame
	void Update () {  

        if (Input.GetMouseButtonDown(1))
        {
            //anim.SetInteger("AIndex", 1);
            playerState.CurState = PlayerState.PState.Moving;
            if (ActiveMarker != null)
            {
                Destroy(ActiveMarker);
                ActiveMarker = null;
            }

            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
            {
                agent.destination = hit.point;
                ActiveMarker = Instantiate(Marker,hit.point,transform.rotation);
            }
        }
        
        if (ActiveMarker != null)
        {
            dist = Vector3.Distance(ActiveMarker.transform.position, gameObject.transform.position);
            if (playerState.CurState != PlayerState.PState.Moving)
            {
                Destroy(ActiveMarker);
                ActiveMarker = null;                
            }

            if (dist <=  1)
            {
                Destroy(ActiveMarker);
                ActiveMarker = null;
                //anim.SetInteger("AIndex", 0);
                playerState.CurState = PlayerState.PState.Idle;
            }
        }        
    }

    public void MoveToTarget(GameObject target)
    {
        playerState.CurState = PlayerState.PState.Moving;
        agent.destination = target.transform.position;  
    }    
}
