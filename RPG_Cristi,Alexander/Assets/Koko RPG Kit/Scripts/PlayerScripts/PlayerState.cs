﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour {
    Animator anim;

    public enum PState
    {
        Moving, 
        Attacking,
        Idle,
        TakeDam,
        Skill
    }
    public PState CurState;

	// Use this for initialization
	void Start () {
        
        //CurState = PState.Idle;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {      
        CheckCurState();       
    }  

    void CheckCurState()
    {
        switch (CurState)
        {
            case PState.Idle:
                Idle();
                break;

            case PState.Moving:
                Moving();
                break;

            case PState.Attacking:
                Attack();
                break;

            case PState.TakeDam:
                TakeDam();
                break;

            case PState.Skill:
                Skill();
                break;
        }

    }

   

    void Idle()
    {
        anim.SetInteger("AIndex", 0);
    }

    void Moving()
    {
        anim.SetInteger("AIndex", 1);   
    }

    void Attack()
    {
        anim.SetInteger("AIndex", 4);
    }

    void TakeDam()
    {
        anim.SetInteger("AIndex", 5);
    }

    void Skill()
    {
        anim.SetInteger("AIndex", 6);
    }

}

