﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest : MonoBehaviour {
    public bool CanOpen;
    float distanceToPlayer;

    public bool Activated;

    public GameObject QuestDialogMenu;

    public GameObject Q1, Q2;
	// Use this for initialization
	void Start () {
        QuestDialogMenu.SetActive(false);
        Activated = false;

        Q1.SetActive(false);
        Q2.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        CheckPlayer();
        if(!CanOpen)
        {
            QuestDialogMenu.SetActive(false);
        }

    }

    private void OnMouseDown()
    {
        if(CanOpen && !Activated)
            QuestDialogMenu.SetActive(true);
    }

    void CheckPlayer()
    {
        Collider[] Range = Physics.OverlapSphere(transform.position, 7);
        for (int i = 0; i < Range.Length; i++)
        {
            if (Range[i].tag == "Player")
            {

                distanceToPlayer = Vector3.Distance(transform.position, Range[i].transform.position);
                if (distanceToPlayer < 3)
                    CanOpen = true;

                else
                {
                    CanOpen = false;
                }

            }
        }

    }

    public void Activate()
    {
        QuestDialogMenu.SetActive(false);
        Activated = true;

        Q1.SetActive(true);
        Q1.GetComponent<QuestItem>().Activated = true;

        Q2.SetActive(true);
        Q2.GetComponent<QuestItem>().Activated = true;
    }
}
