﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestItem : MonoBehaviour {

    public Text Counter;   
    public bool Activated;
    Store str;
    public PlayerStatsHolder PSH;
    public int MaxNum, curNum;

    public Quest Q;

    public GameObject Reward1, Reward2, Reward3;

    // Use this for initialization
    protected virtual void Start () {
        Activated = false;
        Q = GameObject.Find("Quest").GetComponent<Quest>();
	}

    // Update is called once per frame
    protected virtual void Update () {  

       
    }

    public virtual void Claim()
    {
        if (Q.CanOpen)
        {
            if (curNum >= MaxNum)
            {
                //todo rewards
                GameObject P = GameObject.FindGameObjectWithTag("Player");
                PSH = GameObject.Find("StatsHolder").GetComponent<PlayerStatsHolder>();
                Instantiate(Reward1, P.transform.position, Quaternion.identity);
                Instantiate(Reward2, P.transform.position, Quaternion.identity);
                Instantiate(Reward3, P.transform.position, Quaternion.identity);

                PSH.gold += 200;
                gameObject.SetActive(false);

                Debug.Log("Rewarded");
            }

            else
            {
                Debug.Log("not available");
            }
        }

        else
        {
            Debug.Log("Not in range");
        }

    }
}    