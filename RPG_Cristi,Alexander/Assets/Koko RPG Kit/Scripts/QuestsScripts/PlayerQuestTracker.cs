﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerQuestTracker : MonoBehaviour {
    Quest Q;
    public GameObject Q1, Q2;    

	// Use this for initialization
	void Start () {
        Q = GameObject.Find("Quest").GetComponent<Quest>();

        Q1 = Q.Q1;
        Q2 = Q.Q2;
    }
	
	// Update is called once per frame
	void Update () {		
    }
       
    public void KillTrack(GameObject Mon)
    {
        Debug.Log("Tracked");
        if(Q1.GetComponent<QuestItem>().Activated)
        {
            Q1.GetComponent<QuestItem>().curNum++;

        }

        if (Q2.GetComponent<QuestItem>().Activated && 
            Mon.GetComponent<MonsterAI>().MonsterType == MonsterAI.MonType.Slime)
        {
            Q2.GetComponent<QuestItem>().curNum++;

        }
    }
}
