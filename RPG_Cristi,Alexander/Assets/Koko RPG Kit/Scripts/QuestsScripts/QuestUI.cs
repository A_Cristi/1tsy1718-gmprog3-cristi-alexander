﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestUI : MonoBehaviour {

    bool IsHidden;

    RectTransform thisRect;
    Vector3 move;


    // Use this for initialization
    void Start () {
        IsHidden = true;
        thisRect = gameObject.GetComponent<RectTransform>();
        move = new Vector3(0, 500, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (IsHidden == false)
            {
                thisRect.localPosition += move;
                IsHidden = true;
            }
            else
            {
                thisRect.localPosition -= move;
                IsHidden = false;
            }
        }
    }
}
    