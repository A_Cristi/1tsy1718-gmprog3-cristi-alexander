﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsHolder : MonoBehaviour {
    public Image HPBar;
    public Image ExpBar;
    public Image MPBar;

    public Text ExpText;
    public Text MAxExpText;
    public Text Level;
    public Text HP;
    public Text Strength;
    public Text Vitality;
    public Text ATKdmg;
    public Text StatPoints;
    public Text ManaText;
    public Text MatkText;
    public Text MPregenText;


    public float curExp;
    float maxExp;
    int PlayerLevel;
    public float atkdmg;

    public int gold;
    public Text GoldTxt;

    public float curHP, maxHP;
    public float CurMP, MaxMP, MPRegen, Matk;

    public float BaseVitality, BaseStrength;
    public float BaseInt, BaseSpirit;

    public int Lives;
        
    public GameObject PlayerPrefab;
    public GameObject RespawnLoc;

    public bool HasLvlUp;

    int statPoints;

    public GameObject STR, VIT;

    GameObject player;

    float timer;
    bool blessingActive = false;

    public bool VITBuffActive = false;
    bool vitStarted = false;
    float VitTimer = 20.0f;

    public bool STRBuffActive = false;
    float StrTimer = 20.0f;
    bool strStarted = false;

    public bool INTBuffActive = false;
    float IntTimer = 20.0f;
    bool intStarted = false;

    public bool SPRBuffActive = false;
    float SprTimer = 20.0f;
    bool sprStarted = false;

    // Use this for initialization
    void Start () {
        HPBar.fillAmount = 1.0f;
        ExpBar.fillAmount = 0.0f;
        MPBar.fillAmount = 1.0f;

        curExp = 0;
        PlayerLevel = 1;
        maxExp = PlayerLevel * 100.0f;

        HasLvlUp = false;

        statPoints = 0;
        timer = 10.0f;

        player = GameObject.FindGameObjectWithTag("Player");

        STR.SetActive(false);
        VIT.SetActive(false);        
    }
	
	// Update is called once per frame
	void Update () {
        CheckExp();

        ExpText.text = "EXP : " + curExp;
        MAxExpText.text = "Max EXP : " + maxExp;
        Level.text = "Level : " + PlayerLevel;
        HP.text = "HP : " + curHP + " / " + maxHP;

        Strength.text = "Str : " + BaseStrength;
        Vitality.text = "Vit : " + BaseVitality;
        ATKdmg.text = "ATK : " + atkdmg;

        StatPoints.text = "ST PTS : " + statPoints;

        GoldTxt.text = "Gold : " + gold;

        ManaText.text = "MP : " + CurMP + " / " + MaxMP;
        MatkText.text = "Matk : " + Matk;
        MPregenText.text = "+ " + MPRegen;

        CheckStatPoints();

        Activate();
        CheckBuffs();

        if (curHP <= 0 && player != null)
        {   
            Death();
        }

        if (blessingActive == true)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                blessingActive = false;
                timer = 10.0f;
                BaseStrength -= 10.0f;
                BaseInt -= 10.0f;
                player.GetComponent<PlayerStats>().Buffed();
            }
        }
    }

    public void Death()
    {       
        Destroy(player);
        Instantiate(PlayerPrefab, RespawnLoc.transform.position, Quaternion.identity);
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void UpdateHPBar(float maxHP, float curHP)
    {
        HPBar.fillAmount = (curHP / maxHP);
    }

    public void UpdateMPBar(float maxMP, float curMP)
    {
        MPBar.fillAmount = (curMP / maxMP);
    }

    void CheckStatPoints()
    {
        if (statPoints > 0)
        {
            STR.SetActive(true);
            VIT.SetActive(true);
        }

        else
        {
            STR.SetActive(false);
            VIT.SetActive(false);
        }
    }

    void CheckExp()
    {
        ExpBar.fillAmount = curExp / maxExp;
        maxExp = PlayerLevel * 100.0f;
      
        if (curExp >= maxExp)
        {
            float SpillEXP = curExp - maxExp;
            
            Debug.Log("level");
            curExp = 0;
            curExp += SpillEXP;

            statPoints++;
            PlayerLevel += 1;
        }
    }  

    public void AddSTR()
    {
        BaseStrength += .25f;
        player.GetComponent<PlayerStats>().lvlup();
        statPoints--;
    }

    public void AddVIT()
    {
        BaseVitality += .25f;
        player.GetComponent<PlayerStats>().lvlup();
        statPoints--;
    }

    public void Blessing()
    {
        blessingActive = true;
        BaseStrength += 10.0f;
        BaseInt += 10.0f;
        player.GetComponent<PlayerStats>().Buffed();
    }

    public void CheckBuffs()
    {
        if(VITBuffActive && !vitStarted)
        {
            BaseVitality += 10;
            vitStarted = true;
            Debug.Log("VIT");
            player.GetComponent<PlayerStats>().CheckBuff();
        }

        if(STRBuffActive && !strStarted)
        {
            BaseStrength += 10;
            strStarted = true;
            Debug.Log("STR");
            player.GetComponent<PlayerStats>().CheckBuff();
        }

        if(INTBuffActive && !intStarted)
        {
            BaseInt += 10;
            intStarted = true;
            Debug.Log("INT");
            player.GetComponent<PlayerStats>().CheckBuff();
        }

        if (SPRBuffActive && !sprStarted)
        {
            BaseSpirit += 10;
            sprStarted = true;
            Debug.Log("SPR");
            player.GetComponent<PlayerStats>().CheckBuff();
        }
    }

    public void Activate()
    {
        if (VITBuffActive && vitStarted)
        {
            VitTimer -= Time.deltaTime;
            //Debug.Log(VitTimer);
            if(VitTimer <= 0)
            {
                vitStarted = false;
                VITBuffActive = false;

                BaseVitality -= 10;
                VitTimer = 20;
                player.GetComponent<PlayerStats>().CheckBuff();
            }
        }

        if (STRBuffActive && strStarted)
        {
            StrTimer -= Time.deltaTime;
            //Debug.Log(StrTimer);
            if (StrTimer <= 0)
            {
                strStarted = false;
                STRBuffActive = false;

                BaseStrength -= 10;
                StrTimer = 20;
                player.GetComponent<PlayerStats>().CheckBuff();
            }
        }

        if (INTBuffActive && intStarted)
        {
            IntTimer -= Time.deltaTime;
            //Debug.Log(StrTimer);
            if (StrTimer <= 0)
            {
                intStarted = false;
                INTBuffActive = false;

                BaseInt -= 10;
                IntTimer = 20;
                player.GetComponent<PlayerStats>().CheckBuff();
            }
        }

        if (SPRBuffActive && sprStarted)
        {
            SprTimer -= Time.deltaTime;
            //Debug.Log(StrTimer);
            if (SprTimer <= 0)
            {
                sprStarted = false;
                SPRBuffActive = false;

                BaseSpirit -= 10;
                SprTimer = 20;
                player.GetComponent<PlayerStats>().CheckBuff();
            }
        }

    }

}
