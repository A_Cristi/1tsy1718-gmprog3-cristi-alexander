﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreUI : MonoBehaviour {

    bool IsHidden;

    RectTransform thisRect;
    Vector3 move;

    public Store store;

    // Use this for initialization
    void Start () {
        IsHidden = true;
        thisRect = gameObject.GetComponent<RectTransform>();

        //store = GameObject.Find("Store").GetComponent<Store>();

        move = new Vector3(475, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.E) && store.CanOpen)
        {
            if (IsHidden == false)
            {
                thisRect.localPosition += move;
                IsHidden = true;
            }
            else
            {
                thisRect.localPosition -= move;
                IsHidden = false;
            }
        }

        if(!IsHidden && !store.CanOpen)
        {
            thisRect.localPosition += move;
            IsHidden = true;
        }
    }
    
}
