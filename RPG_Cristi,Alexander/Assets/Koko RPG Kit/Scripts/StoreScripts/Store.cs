﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Store : MonoBehaviour {

    public bool CanOpen;
    float distanceToPlayer;
    public PlayerStatsHolder PSH;

    public GameObject HPP, MPP, VITB, STRB, SPRB, INTB;

    // Use this for initialization
    void Start () {      
        CanOpen = false;
	}
	
	// Update is called once per frame
	void Update () {
        CheckPlayer();
    }

    void CheckPlayer()
    {
        Collider[] Range = Physics.OverlapSphere(transform.position, 7);
        for (int i = 0; i < Range.Length; i++)
        {
            if(Range[i].tag == "Player")
            {             

                distanceToPlayer = Vector3.Distance(transform.position, Range[i].transform.position);
                if (distanceToPlayer < 3)
                    CanOpen = true;

                else
                {
                    CanOpen = false;                   
                }
                    
            }
        }

    }

    public void BuyHP()
    {
        GameObject P = GameObject.FindGameObjectWithTag("Player");

        if(PSH.gold >= 100)
        {
            Instantiate(HPP, P.transform.position, Quaternion.identity);
            PSH.gold -= 100;
        }   
    }

    public void BuyMP()
    {
        GameObject P = GameObject.FindGameObjectWithTag("Player");

        if (PSH.gold >= 100)
        {
            Instantiate(MPP, P.transform.position, Quaternion.identity);
            PSH.gold -= 100;
        }
    }

    public void BuyVITB()
    {
        GameObject P = GameObject.FindGameObjectWithTag("Player");

        if (PSH.gold >= 250)
        {
            Instantiate(VITB, P.transform.position, Quaternion.identity);
            PSH.gold -= 250;
        }
    }

    public void BuySTRB()
    {
        GameObject P = GameObject.FindGameObjectWithTag("Player");

        if (PSH.gold >= 250)
        {
            Instantiate(STRB, P.transform.position, Quaternion.identity);
            PSH.gold -= 250;
        }
    }

    public void BuyINTB()
    {
        GameObject P = GameObject.FindGameObjectWithTag("Player");

        if (PSH.gold >= 250)
        {
            Instantiate(INTB, P.transform.position, Quaternion.identity);
            PSH.gold -= 250;
        }
    }

    public void BuySPRB()
    {
        GameObject P = GameObject.FindGameObjectWithTag("Player");

        if (PSH.gold >= 250)
        {
            Instantiate(SPRB, P.transform.position, Quaternion.identity);
            PSH.gold -= 250;
        }
    }   

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 3);
    }
}
